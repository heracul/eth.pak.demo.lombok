package eth.pak.demo.lombok.model;

import eth.pak.demo.lombok.annotation.EthGetter;
import lombok.Getter;
import lombok.Setter;

public class EthVo {

    private String firstName;
    @Getter
    @Setter
    private String lastName;
    @Getter
    @Setter
    private int age;
    private int height;
    private int weight;

    @EthGetter
    public String getFirstName() {
        return firstName;
    }
}
