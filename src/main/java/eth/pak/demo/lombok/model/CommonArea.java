package eth.pak.demo.lombok.model;

import eth.pak.demo.lombok.annotation.MapGetter;

import java.util.HashMap;
import java.util.Map;

@MapGetter(ref = "commonAreaMap")
public class CommonArea {
    private final Map<String, Object>commonAreaMap;
    private String guid;
    private String srvcId;
    private String trnmChnlCd;
    private String wasInstNm;
    private long amount;

    public CommonArea() {
        this.commonAreaMap = new HashMap<>();
    }

//    public String getGuid() {
//        return (String) commonAreaMap.getOrDefault("guid", "");
//    }
}
